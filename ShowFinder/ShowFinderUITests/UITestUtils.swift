//
//  UITestUtils.swift
//  ShowFinderUITests
//
//  Created by Jagan Moorthy on 19/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import XCTest

struct UITestUtils{
    static func inputQuery(query: String, app: XCUIApplication){
        let searchTextField = app.textFields[Constants.AccessibilityLabels.showListScreen.searchTextField]
        searchTextField.tap()
        searchTextField.typeText(query)
        sleep(1)
    }
        
    static var currentLanguage: (langCode: String, localeCode: String)? {
        let currentLocale = Locale(identifier: Locale.preferredLanguages.first!)
        guard let langCode = currentLocale.languageCode else {
            return nil
        }
        var localeCode = langCode
        if let scriptCode = currentLocale.scriptCode {
            localeCode = "\(langCode)-\(scriptCode)"
        } else if let regionCode = currentLocale.regionCode {
            localeCode = "\(langCode)-\(regionCode)"
        }
        return (langCode, localeCode)
    }
    
    static func localizedString(_ key: String, classType: AnyClass) -> String {
        let testBundle = Bundle(for: /* a class in your test bundle */classType)
        if let currentLanguage = currentLanguage,
            let testBundlePath = testBundle.path(forResource: currentLanguage.localeCode, ofType: "lproj") ?? testBundle.path(forResource: currentLanguage.langCode, ofType: "lproj"),
            let localizedBundle = Bundle(path: testBundlePath)
        {
            return NSLocalizedString(key, bundle: localizedBundle, comment: "")
        }
        return key
    }
}
