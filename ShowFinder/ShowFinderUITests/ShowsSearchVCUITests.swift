//
//  ShowsSearchVCUITests.swift
//  ShowFinderUITests
//
//  Created by Jagan Moorthy on 11/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import XCTest

final class ShowsSearchVCUITests: XCTestCase{
    var app:XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSubViewsExist(){
        XCTAssert(app.tables["searchShowsTable"].staticTexts.count == 0)
        XCTAssertTrue(app.staticTexts[Constants.AccessibilityLabels.showListScreen.infoLabel].exists)
        XCTAssertEqual(app.staticTexts[Constants.AccessibilityLabels.showListScreen.infoLabel].value as! String, UITestUtils.localizedString("Enter a search term", classType: ShowsSearchVCUITests.self) )
    }
    
    
    
    func testSearch(){
        let query = "How I Met Your Mother"
        UITestUtils.inputQuery(query: query, app: app)
        XCTAssertTrue(app.tables["searchShowsTable"].staticTexts[query].exists)
        XCTAssertFalse(app.staticTexts["Enter a search term"].exists)
        XCTAssert(app.tables["searchShowsTable"].staticTexts.count > 0)
    }
    
    func testNoReuslts(){
        let query = "Fr"
        UITestUtils.inputQuery(query: query, app: app)
        XCTAssert(app.tables["searchShowsTable"].staticTexts.count == 0)
       
       let localizedMessage = UITestUtils.localizedString("No results. Try a different search term.", classType: ShowsSearchVCUITests.self)
        XCTAssertEqual(app.staticTexts[Constants.AccessibilityLabels.showListScreen.infoLabel].value as! String, localizedMessage)
    }
    
    func testTextFieldClear(){
        let query = "Grey's"
        UITestUtils.inputQuery(query: query, app: app)
        XCTAssert(app.tables["searchShowsTable"].staticTexts.count > 0)
            app.buttons[UITestUtils.localizedString("Clear text", classType: ShowsSearchVCUITests.self)].tap()
        
        XCTAssert(app.tables["searchShowsTable"].staticTexts.count == 0)
        XCTAssertEqual(app.staticTexts[Constants.AccessibilityLabels.showListScreen.infoLabel].value as! String, UITestUtils.localizedString("Enter a search term", classType: ShowsSearchVCUITests.self))
        
        
    }

}
