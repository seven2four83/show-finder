//
//  ShowDetailsVCUITests.swift
//  ShowFinderUITests
//
//  Created by Jagan Moorthy on 19/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

final class ShowDetailsVCUITests: XCTestCase {

    var app:XCUIApplication!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        let query = "How I Met Your Mother"
        UITestUtils.inputQuery(query: query, app: app)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAllSubviews(){
        
        app.tables["searchShowsTable"]/*@START_MENU_TOKEN@*/.staticTexts["How I Met Your Mother"]/*[[".cells.staticTexts[\"How I Met Your Mother\"]",".staticTexts[\"How I Met Your Mother\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(1)
        let elementsQuery = app.scrollViews.otherElements
        XCTAssertTrue(elementsQuery.otherElements["imageHolderAccessibilityLabel"].exists)
        XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["titleAccessibilityLabel"]/*[[".staticTexts[\"How I Met Your Mother\"]",".staticTexts[\"titleAccessibilityLabel\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.exists)
        XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["typeDetailAccessibilityLabel"]/*[[".staticTexts[\"Scripted\"]",".staticTexts[\"typeDetailAccessibilityLabel\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.exists)
        XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["typeAccessibilityLabel"]/*[[".staticTexts[\"Type:\"]",".staticTexts[\"typeAccessibilityLabel\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.exists)
        XCTAssertTrue(elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["languageAccessibilityLabel"]/*[[".staticTexts[\"Language:\"]",".staticTexts[\"languageAccessibilityLabel\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.exists)
        XCTAssertTrue(elementsQuery.staticTexts["languageDetailAccessibilityLabel"].exists)
        XCTAssertTrue(elementsQuery.staticTexts["summaryAccessibilityLabel"].exists)
        
        XCTAssertEqual(elementsQuery.staticTexts[Constants.AccessibilityLabels.ShowDetailsScreen.title].value as! String, "How I Met Your Mother")
        XCTAssertEqual(elementsQuery.staticTexts[Constants.AccessibilityLabels.ShowDetailsScreen.typeDetail].value as! String, "Scripted")
        
        XCTAssertEqual(elementsQuery.staticTexts[Constants.AccessibilityLabels.ShowDetailsScreen.languageDetail].value as! String, "English")
        
        XCTAssertEqual(elementsQuery.staticTexts[Constants.AccessibilityLabels.ShowDetailsScreen.summary].value as! String, "How I Met Your Mother is a comedy about Ted and how he fell in love. It all starts when Ted\'s best friend, Marshall drops the bombshell that he\'s going to propose to his long-time girlfriend, Lilya kindergarten teacher. At that moment, Ted realizes that he had better get a move on if he too hopes to find true love. Helping him in his quest is Barney a friend with endless, sometimes outrageous opinions, a penchant for suits and a foolproof way to meet women. When Ted meets Robin he\'s sure it\'s love at first sight, but destiny may have something else in store.\n")
    }

}
