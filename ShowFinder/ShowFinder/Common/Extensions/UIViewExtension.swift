//
//  UIViewExtension.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 14/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func addBlur(_ alpha: CGFloat = 0.5) {
        // create effect
        let effect = UIBlurEffect(style: .dark)
        let effectView = UIVisualEffectView(effect: effect)
        
        // set boundry and alpha
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha
        
        self.addSubview(effectView)
    }
}

