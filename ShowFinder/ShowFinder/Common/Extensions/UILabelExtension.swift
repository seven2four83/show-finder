//
//  UILabelExtension.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 14/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    func enableMultiLine(lineBreakMode: NSLineBreakMode = NSLineBreakMode.byWordWrapping){
        self.lineBreakMode = .byWordWrapping
        self.numberOfLines = 0
        self.sizeToFit()
    }
    func setTextAndAccessibilityValue(text: String){
        self.text = text
        self.accessibilityValue = text
    }
    
    @IBInspectable var xibLocalizationKey: String? {
        get { return nil }
        set(key) {
            guard let key = key else {
                text = nil
                return
            }
            text = key.localized
        }
    }
}
