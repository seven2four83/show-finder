//
//  StringExtension.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 15/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

extension String{
    func stripHTMLTags() -> String?{
        guard let data = data(using: .utf8) else {
            return nil
        }
        return try? (NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes:nil)).string
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

