//
//  Constants.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 16/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

///Constants to be used across the app
struct Constants{
    /// Used for XCTests
    struct AccessibilityLabels{
        struct ShowDetailsScreen {
            struct ImageHolder {
                static let scaledImage = "scaledImageAccessibilityLabel"
                static let aspectFitImage = "aspectFitImageAccessibilityLabel"
//                static let placeholderImage = "placeholderImageAccessibilityLabel"
            }
            static let imageHolderLabel = "imageHolderAccessibilityLabel"
            static let title = "titleAccessibilityLabel"
            static let type = "typeAccessibilityLabel"
            static let typeDetail = "typeDetailAccessibilityLabel"
            static let language = "languageAccessibilityLabel"
            static let languageDetail = "languageDetailAccessibilityLabel"
            static let summary = "summaryAccessibilityLabel"
        }
        struct showListScreen{
            static let searchTextField = "searchTextField"
            static let infoLabel = "listInfoLabel"
        }
    }
}
