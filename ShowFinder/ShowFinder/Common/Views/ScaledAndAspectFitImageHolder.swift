//
//  ScaledAndAspectFitImage.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 14/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()
/// An image view which sets the backgorund to be scale-to-fit versions of the image. Uses two UIImageViews, XIB for more details
final class ScaledAndAspectFitImageHolder: UIView {
    
    private var image: UIImage? = nil{
        willSet{
            scaledImageView.image = newValue
            scaledImageView.addBlur()
            aspectFitImageView.image = newValue
        }
    }
    
    @IBOutlet var scaledImageView: UIImageView!
    @IBOutlet var aspectFitImageView: UIImageView!
    
    @IBOutlet var infoLabel: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit();
    }
    
    private func customInit(){
        if let contentView = Bundle.main.loadNibNamed("ScaledAndAspectFitImageHolder", owner: self, options: nil)?.first as? UIView{
            contentView.frame = self.bounds
            self.addSubview(contentView);
        }
    }
    
    func imageFromServerURL(_ URLString: String, placeHolder: UIImage?) {
        self.image = placeHolder
        if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
            self.image = cachedImage
            return
        }
        if let url = URL(string: URLString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    self.infoLabel.text = "No image found".localized
                    self.infoLabel.sizeToFit()
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            self.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}
