//
//  UIUtility.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

///General utility for UI
struct UIUtility {
    static func showSingleActionAlert(title: String, message:String, viewController:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK".localized, style: .cancel)
        alert.addAction(okayAction)
        DispatchQueue.main.async {
            viewController.present(alert, animated: false, completion: nil)
        }
    }
}
