//
//  BaseShow.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

///Concrete class representing a show.
struct BaseShow : Show{
    var name: String = ""
    var language: String = ""
    var originalImageURL: String? = nil
    var thumbnailImage: UIImage? = nil
    var type: String = ""
    var summary: String = ""
    
    //TODO: Decodable
//    var thumbnailImageURL: String
//    enum CodingKeys : String, CodingKey {
//        case name
//        case language
//        case originalImageURL =
//    }
    
}
