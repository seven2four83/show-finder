//
//  Show.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 11/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

//TODO: Decodable
///Protocol representing a show.
protocol Show {
    var name:String {get set}
    var language: String {get set}
    var thumbnailImage: UIImage? {get set}
    var originalImageURL: String? {get set}
    var type: String {get set}
    var summary: String {get set}
}
