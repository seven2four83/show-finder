//
//  AppDelegate.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 11/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit
import CoreData

// AppDelegate for the app. Try to not do any important stuff directly here. Use specific handlers if it's not really AppDelegate's responsibility
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

