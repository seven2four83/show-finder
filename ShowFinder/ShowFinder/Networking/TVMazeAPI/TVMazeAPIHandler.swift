//
//  TVMazeAPIHandler.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 11/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

///Handles communication with TVMaze API. Requests and responses available at http://www.tvmaze.com/api
struct TVMazeAPIHandler: ShowsFetcher{
    let urlSession = URLSession.shared
    let viewController: UIViewController
    
    ///Throws an error alert if enocding fails or data returned from the server is unreadable
    func fetchShows(query: String, completionHandler: @escaping ([Show]) -> (),  shouldEncode: Bool = true) {
        var finalQuery = query
        if shouldEncode{
            guard let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else{
                return
            }
            finalQuery = encodedQuery
        }
        guard let queryURL = URL(string: APIConstants.URLStrings.showSearch + finalQuery) else {
            UIUtility.showSingleActionAlert(title: "Error".localized, message: "Error fetching data".localized, viewController: viewController)
            return
        }
        let dataTask = urlSession.dataTask(with: queryURL) { (data, response, error) in
            guard let data = data, let shows = TVShowsResponseHandler.parseResponse(data: data) else{
                UIUtility.showSingleActionAlert(title:  "Error".localized, message: "Error fetching data".localized, viewController: self.viewController)
                return
            }
            completionHandler(shows)
        }
        dataTask.resume()
    }
}

extension TVMazeAPIHandler{
    private  struct APIConstants {
         struct URLStrings {
            static let baseURL = "https://api.tvmaze.com"
            static let showSearch = "\(APIConstants.URLStrings.baseURL)/search/shows?q="
        }
    }
}
