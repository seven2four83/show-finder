//
//  TVShowsResponseHandler.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

///Handles responses from TVMaze API. Handles communication with TVMaze API. Requests and responses available at http://www.tvmaze.com/api
struct TVShowsResponseHandler {
    
    static func parseResponse(data: Data) -> [Show]? {
        guard let jsonEntities = try? JSONSerialization.jsonObject(with: data, options: []) as? [Any] else{
            return nil
        }
        var shows = [Show]()
        for case let showWithScore as [String:Any]  in jsonEntities{
                if let showDict = showWithScore[JSONKeys.showEntity] as? [String:Any] {
                    shows.append(getShow(fromShowDict: showDict))
                }
        }
        return shows
    }
    
    private static func getShow(fromShowDict showDict: [String:Any]) -> Show{
        var show = BaseShow()
        show.name = getValidValue(fromShowDict: showDict, forKey: JSONKeys.Show.name) ?? "No name".localized
        show.language = getValidValue(fromShowDict: showDict, forKey: JSONKeys.Show.language) ?? "Unknown".localized
        show.summary = getValidValue(fromShowDict: showDict, forKey: JSONKeys.Show.summary) ?? "No information available".localized
        show.type = getValidValue(fromShowDict: showDict, forKey: JSONKeys.Show.type) ?? "Unknown".localized
        if let images = showDict[JSONKeys.Show.images] as? [String:String]{
            if let thumbnailImageURLString = changeProtocolToHttps(forUrlString: images[JSONKeys.Show.thumbnail]), let thumbnailImageURL = URL(string: thumbnailImageURLString){
                show.thumbnailImage = try? UIImage(data: Data(contentsOf: thumbnailImageURL))
            }else{
                show.thumbnailImage = #imageLiteral(resourceName: "unknown_show_30.png")
            }
            show.originalImageURL = changeProtocolToHttps(forUrlString:images[JSONKeys.Show.fullImage])
        }else{
            show.thumbnailImage = #imageLiteral(resourceName: "unknown_show_30.png")
        }
        return show
    }
    
    static func changeProtocolToHttps(forUrlString urlString: String?) -> String?{
        return urlString?.replacingOccurrences(of: "http", with: "https")
    }
    
    
    static func getValidValue(fromShowDict showDict: [String:Any], forKey key:String) -> String?{
        guard let value = showDict[key] as? String, value.count > 0 else {
            return nil
        }
        return value
    }
    
}

extension TVShowsResponseHandler{
    private struct JSONKeys{
        static let showEntity = "show"
        struct Show {
            static let name = "name"
            static let language = "language"
            static let images = "image"
            static let thumbnail = "medium"
            static let fullImage = "original"
            static let summary = "summary"
            static let type = "type"
        }
    }
}
