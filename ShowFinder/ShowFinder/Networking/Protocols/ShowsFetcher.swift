//
//  ShowsFetcher.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 11/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

///Protocol for any API that can return TV shows based on a given query
protocol ShowsFetcher {
    func fetchShows(query: String, completionHandler: @escaping ([Show]) -> (), shouldEncode: Bool)
}
