//
//  ShowDetailViewController.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 13/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit

/// Displays the details of a particular show. Uses separate XIB. Must set the "show" attribute, else will present an error alert
final class ShowDetailViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var typeDetailLabel: UILabel!
    @IBOutlet var languageDetailLabel: UILabel!
    
    @IBOutlet var imageView: ScaledAndAspectFitImageHolder!
    var show:Show? = nil

}

extension ShowDetailViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureSubViews()
        title = "Show Details".localized
    }
    
    private func configureSubViews(){
        guard  let show = show else {
            UIUtility.showSingleActionAlert(title: "Error".localized, message: "Error displaying data".localized, viewController: self)
            return
        }
        if let originalImageURL = show.originalImageURL{
            imageView.imageFromServerURL(originalImageURL, placeHolder: show.thumbnailImage)
        }else{
            imageView.infoLabel.text = "No image found".localized
            imageView.infoLabel.sizeToFit()
        }
        titleLabel.setTextAndAccessibilityValue(text: show.name)
        titleLabel.enableMultiLine()
        languageDetailLabel.setTextAndAccessibilityValue(text: show.language)
        typeDetailLabel.setTextAndAccessibilityValue(text: show.type)
        configureSummaryLabel(show: show)
    }
    
    private func configureSummaryLabel(show: Show){
        if let plainText = show.summary.stripHTMLTags(){
            summaryLabel.setTextAndAccessibilityValue(text: plainText)
        }else{
            summaryLabel.setTextAndAccessibilityValue(text: show.summary)
        }
        summaryLabel.enableMultiLine()
    }
}
