//
//  ShowsSearchViewController.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 11/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/// Home screen of the app. Has a textbox to search for shows. The results are showed in a tableView. Uses stroyboard
final class ShowsSearchViewController:UIViewController {
    
    private struct ShowsListVCConstants{
        struct searchResultsTable {
            static let cellReuseIdentifier = "showCell"
        }
    }
    
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var searchResultsTable: UITableView!
    @IBOutlet var infoLabel: UILabel!
    
    lazy var showsFetcher: ShowsFetcher = TVMazeAPIHandler(viewController: self)
    private let disposeBag = DisposeBag()
    private let throttleInterval = 300
    private let shows:BehaviorRelay<[Show]> = BehaviorRelay(value: [])
}

//MARK: - View lifecylce
extension ShowsSearchViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        stripDummyHTMLStringForPerformanceImproevement()
        setupSearchTextField()
        setupSearchResultsTable()
        setupSearchResultsTableCellTapHandling()
        title = "Search TV Shows".localized
    }
}

extension ShowsSearchViewController{
    private func setupSearchTextField(){
        let queryInput = searchTextField.rx.text.observeOn(MainScheduler.asyncInstance).distinctUntilChanged().throttle(.milliseconds(throttleInterval), scheduler: MainScheduler.instance)
        queryInput.subscribe(onNext: queryInputDidChange(value:)).disposed(by: disposeBag)
    }
    
    func queryInputDidChange(value: String?){
        guard let queryString = value else{
            UIUtility.showSingleActionAlert(title: "Error".localized, message: "Error reading input".localized, viewController: self)
            return
        }
        if queryString.count > 0{
            showsFetcher.fetchShows(query: queryString, completionHandler: self.searchCompletionHandler(shows:),shouldEncode: true)
        }else{
            searchCompletionHandler(shows: [Show]())
        }
    }
    
    private func searchCompletionHandler(shows: [Show]){
        self.shows.accept(shows)
        self.configureInfoLabel()
    }
    
    private func setupSearchResultsTable(){
        shows.bind(to: searchResultsTable.rx.items(cellIdentifier: ShowsListVCConstants.searchResultsTable.cellReuseIdentifier)){
            row, show, cell in
            cell.textLabel?.text = show.name
            cell.imageView?.image = show.thumbnailImage
            cell.imageView?.contentMode = .scaleToFill
            }
        .disposed(by:disposeBag)
        searchResultsTable.tableFooterView = UIView()
    }
    
    private func configureInfoLabel(){
        DispatchQueue.main.async{
            if self.shows.value.count > 0{
                self.infoLabel.isHidden = true
                return
            }else if let length = self.searchTextField.text?.count, length < 1{
                self.infoLabel.setTextAndAccessibilityValue(text:"Enter a search term".localized)
            }else{
                self.infoLabel.setTextAndAccessibilityValue(text: "No results. Try a different search term.".localized)
            }
            self.infoLabel.sizeToFit()
            self.infoLabel.enableMultiLine()
            self.infoLabel.isHidden = false
        }
    }
    
    private func setupSearchResultsTableCellTapHandling(){
        searchResultsTable.rx.modelSelected(Show.self)
            .subscribe(onNext: { [unowned self] show in
                let showDetailsVC = ShowDetailViewController(nibName: "ShowDetailViewController", bundle: nil)
                showDetailsVC.show = show
                self.navigationController?.pushViewController(showDetailsVC, animated: false)
                if let selectedRowIndexPath = self.searchResultsTable.indexPathForSelectedRow {
                    self.searchResultsTable.deselectRow(at: selectedRowIndexPath, animated: true)
                }
            })
        .disposed(by: disposeBag)
    }
}

extension ShowsSearchViewController{
    /// Profiling the app showed that first time stripping of HTML tags takes a lof of time. Hence performing a dummy operation in a separate thread to enhance user experience
    private func stripDummyHTMLStringForPerformanceImproevement(){
        DispatchQueue.global(qos: .default).async {
            let _ = "<p>The city of Townsville may be a beautiful, bustling metropolis, but don't be fooled! There's evil afoot! And only three things can keep the bad guys at bay: Blossom, Bubbles and Buttercup, three super-powered little girls, known to their fans (and villains everywhere) as <b>The Powerpuff Girls</b>. Juggling school, bedtimes, and beating up giant monsters may be daunting, but together the Powerpuff Girls are up to the task. Battling a who's who of evil, they show what it really means to \"fight like a girl.\"</p>".stripHTMLTags()
            
        }
    }
}

