//
//  ShowsSearchViewControllerTests.swift
//  ShowFinderTests
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest
@testable import ShowFinder

final class ShowsSearchViewControllerTests: XCTestCase {
    
    var sut : ShowsSearchViewController!
    var window: UIWindow!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        UIApplication.shared.keyWindow?.rootViewController = sut
//        sut.loadView()
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = (storyboard.instantiateViewController(withIdentifier: "ShowsSearchViewController") as! ShowsSearchViewController)
        loadView()
    }
    
    func loadView()
    {
        window = UIWindow()
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNilQuery() {
        let errorPromise = expectation(description: "Error Alert presented")
        sut.queryInputDidChange(value: nil)
        DispatchQueue.main.async {
            XCTAssertTrue(self.sut.presentedViewController is UIAlertController)
            TestUtility.testErrorAlert(presentationViewController: self.sut,message: "Error reading input".localized)
            errorPromise.fulfill()
        }
        waitForExpectations(timeout: 2, handler: nil)
    }

}
