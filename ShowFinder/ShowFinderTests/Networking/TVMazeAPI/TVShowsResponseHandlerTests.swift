//
//  TVShowsResponseHandlerTests.swift
//  ShowFinderTests
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

final class TVShowsResponseHandlerTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidResponse() {
        guard let path = TestConstants.HardCodedData.validSearchResponse, let data = NSData(contentsOfFile: path) as Data?, let shows = TVShowsResponseHandler.parseResponse(data: data) else {
            XCTAssertFalse(true)
            return
        }
        XCTAssertEqual(shows.count, 10)
        let show = shows[3]
        XCTAssertEqual(show.name, "The Powerpuff Girls")
        XCTAssertEqual(show.language, "English")
        XCTAssertNotNil(show.thumbnailImage)
        //This also ensures the address is changed to https
        XCTAssertEqual(show.originalImageURL, "https://static.tvmaze.com/uploads/images/original_untouched/60/151357.jpg")
    }
    
    func testEmptyValues(){
        guard let path = TestConstants.HardCodedData.validSearchResponse, let data = NSData(contentsOfFile: path) as Data?, let shows = TVShowsResponseHandler.parseResponse(data: data) else {
            XCTAssertFalse(true)
            return
        }
        let show = shows[0]
        XCTAssertEqual(show.name, "No name".localized)
        XCTAssertEqual(show.language, "Unknown".localized)
        XCTAssertEqual(show.type, "Unknown".localized)
        XCTAssertEqual(show.summary, "No information available".localized)
    }
    
    func testInvalidResponse() {
        let data = "".data(using: .utf8)
        XCTAssertNil(TVShowsResponseHandler.parseResponse(data: data!))
    }

    func testHttpsConversion() {
        let validUrl = "http://a"
        XCTAssertEqual(TVShowsResponseHandler.changeProtocolToHttps(forUrlString: validUrl), "https://a")
    }
}
