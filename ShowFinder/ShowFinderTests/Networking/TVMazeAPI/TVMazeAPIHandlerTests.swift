//
//  TVMazeAPIHandlerTests.swift
//  ShowFinderTests
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

final class TVMazeAPIHandlerTests: XCTestCase {
    
    var sut: TVMazeAPIHandler!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let viewController = UIViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
        sut = TVMazeAPIHandler(viewController: viewController)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func testSearchRequest(){
        let searchPromise = expectation(description: "Search request success")
//        sut.fetchShows(query: "2", completionHandler: <#([Show]) -> ()#>) { shows in
//            XCTAssertGreaterThan(shows.count, 0)
//            searchPromise.fulfill()
//        }
        sut.fetchShows(query: "2 Broke", completionHandler: { shows in
            XCTAssertGreaterThan(shows.count, 0)
            searchPromise.fulfill()
        }, shouldEncode: true)
        waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testInvalidURL(){
        sut.fetchShows(query: " a",completionHandler:  { (shows) in
            
        }, shouldEncode: false)
        UIUtilityTests().assertAlert(viewController: sut.viewController, title: "Error".localized, message: "Error fetching data".localized, actionMessage: "OK".localized)
    }
}
