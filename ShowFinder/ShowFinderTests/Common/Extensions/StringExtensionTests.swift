//
//  StringExtensionTests.swift
//  ShowFinderTests
//
//  Created by Jagan Moorthy on 20/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

final class StringExtensionTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testValidDataHTMLStrippng(){
        let htmlString = "<body><p><b>a</b></body>"
        XCTAssertEqual(htmlString.stripHTMLTags(), "a\n")
    }

}
