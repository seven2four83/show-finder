//
//  TestConstants.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

struct TestConstants {
    struct HardCodedData {
        static let validSearchResponse = Bundle.main.path(forResource: "PositiveSearchResult", ofType: "json")
    }
}
