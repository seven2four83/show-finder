//
//  UIUtilityTests.swift
//  ShowFinderTests
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest
import UIKit
@testable import ShowFinder

final class UIUtilityTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSingleActionAlert(){
        let viewController = UIViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
        UIUtility.showSingleActionAlert(title: "testTitle", message: "testMessage", viewController: viewController)
        assertAlert(viewController: viewController, title: "testTitle", message: "testMessage", actionMessage: "OK")
        
    }
    
    func assertAlert(viewController: UIViewController, title: String, message: String, actionMessage: String){
        let promise = expectation(description: "AlertVC is presented")
        DispatchQueue.main.async { XCTAssertTrue(viewController.presentedViewController is UIAlertController)
            let alertVC = viewController.presentedViewController as! UIAlertController
            XCTAssertEqual(alertVC.title, title)
            XCTAssertEqual(alertVC.message, message)
            XCTAssertEqual(alertVC.actions.count, 1)
            let okayAction = alertVC.actions[0]
            XCTAssertEqual(okayAction.title, actionMessage.localized)
            XCTAssertEqual(okayAction.style, .cancel)
            promise.fulfill()
        }
        waitForExpectations(timeout: 20, handler: nil)
        
    }
    
}
