//
//  TestUtility.swift
//  ShowFinder
//
//  Created by Jagan Moorthy on 12/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit
import XCTest

struct TestUtility {
    static func testErrorAlert(presentationViewController viewController:UIViewController, message: String, title:String = "Error".localized, actionString:String = "OK".localized){
        let alertVC = viewController.presentedViewController as! UIAlertController
        
        XCTAssertEqual(alertVC.title, title)
        XCTAssertEqual(alertVC.message, message)
        XCTAssertEqual(alertVC.actions.count, 1)
        
        let okayAction = alertVC.actions[0]
        XCTAssertEqual(okayAction.title, actionString)
        XCTAssertEqual(okayAction.style, .cancel)
    }
}
