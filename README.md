**Show Finder**

An app to search for your favourite shows. Uses the TVMaze API

Note: 
- MVVM architecture available in feat_mvvm branch
- MVC architecture, that I'm most comfortable with is available in master branch. I avoid Massive VC syndrome by limiting files to 100 lines of code. More info available in "Key Design/Implementation Decisions" section
- Switching across branches may require a pod install.
- Project uses RxSwift and RxCocoa v5. Please ensure any build errors are not due to older version.
- The project is a bit too simple to have many design decisions. My full list of design decisions are available in the readme file of my [NotesApp](http://https://gitlab.com/seven2four83/notesapp "NotesApp") project. **Also, I'd recommend going through that project code too**.
- Testing was done mostly using **iPhone XR simulator**, but constraints were checked across devices, orientations and adaptations(screen split settings).
- Though the official API page has http endpoints, the app uses http*s* endpoints. Most cases seem to work fine.


**Extra Features**
- Localization: English and Swedish(used Google translate. Kindly excuse any errors)

**Key Design/Implementation Decisions:**

- Basic philosophy: No class knows too much info/implementation detail. A good system is when a lot of dumb classes do something smart together. 
- master branch: Stick to MVC, yet avoid Massive VCs. Less than 100 lines of code/file(This limit applies to non-VC classes too). Primarily only do view setup and interactions. Use handlers for anything else
- Protocol Oriented Programming
- Prefer structs to classes (Exception: Classes handy in XCTest mocking)
- Commenting: If your code needs comments, either the function/variable name is wrong or the function does too much stuff. Rename the function/variable. Code updates, comments dont. Check out Uncle Bob's guidelines(Clean Code) for code comments.
- Prefer dependency injection to singletons
- Functions: Line limit: 15 - 20 lines (exceptions for try-catch and switch blocks)

**TODO:**

- Accessibility support
-  ~5% code coverage missing
- Most test cases only cover positive cases. Add negative cases.

